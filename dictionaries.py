def set_person_data(email, phone, city, street):
    return {
        'contact': {
            'email': email,
            'phone': phone
        },
        'address': {
            'city': city,
            'street': street
        }
    }

if __name__ == '__main__':
    my_pet = {
        'name': 'Krówka',
        'type': 'Cat',
        'age': 8,
        'weight': 10,
        'is_male': True,
        'favourite_food': ['Friskies', 'meat']
    }

    my_pet["is_lazy"] = True
    del my_pet["age"]
    print(my_pet)
    print(set_person_data('test@o2.pl', '34567', 'Warsaw', 'Marszalkowska'))
