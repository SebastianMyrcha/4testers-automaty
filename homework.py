#Zadania domowe 15-04-2024

def describe_game_player(player: dict):
    return (f"The player {player['nick']} is of type {player['type']} "
            f"and has {player['points']} EXP")


def check_if_animal_needs_vaccine(animal_type, animal_age):
    try:
        if animal_type == 'dog':
            return (animal_age - 1) % 2 == 0
        elif animal_type == 'cat':
            return (animal_age - 1) % 3 == 0
        else:
            raise Exception(f"The animal ({animal_type}) is not supported.")
    except Exception as e:
        return e


if __name__ == '__main__':
    print(describe_game_player({'nick': 'Gruby85', 'type': 'warrior', 'points': 4500}))

    print(check_if_animal_needs_vaccine('cat', 4))
    print(check_if_animal_needs_vaccine('dog', 3))
    print(check_if_animal_needs_vaccine('cat', 5))
    print(check_if_animal_needs_vaccine('dog', 1))
    print(check_if_animal_needs_vaccine('fish', 4))
