friends_name = 'Monika'
friends_age = 44
friends_pet_number = 0
has_driving_licence = True
friendship_years = 21.5

print(f'My frined name is {friends_name}.')
print(f'She is {friends_age} years old.')
print("Pet number:", friends_pet_number)
print("Driving licence:", has_driving_licence)
print("Friendship years:", friendship_years)

print("-------------------------------------------")

print(f'My frined name is {friends_name}.', f'She is {friends_age} years old.',
      "Pet number:", friends_pet_number,
      "Driving licence:", has_driving_licence,
      "Friendship years:", friendship_years,
      sep="\n"
      )

name_surname = "Seba Myrcha"
email = "test@o2.pl"
phone = "9876543"

bio = f'''
Name: {name_surname}
Email: {email}
Phone: {phone}
'''

print(bio)

