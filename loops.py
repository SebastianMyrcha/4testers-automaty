import random


def print_ten_numbers():
    for i in range(10):
        print(i)


def print_even_numbers_in_range():
    for i in range(0, 21, 2):
        print(i)


def print_numbers_divisible_by_number_in_range(start, end, divider):
    for i in range(start, end):
        if i % 7 == 0:
            print(i)


def print_random_numbers(n):
    for i in range(n):
        print(random.randint(1, 1000))


def generate_list_of_10_random_numbers():
    return [random.randint(1000, 5000) for x in range(10)]


def print_each_number_in_list_squared(lst):
    for number in lst:
        print(number ** 2)


def convert_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


def map_temperature_from_celsius_to_fahrenheit(lst_of_temperatures_in_celsius):
    return [convert_celsius_to_fahrenheit(temp) for temp in lst_of_temperatures_in_celsius]


def filter_grades_greater_or_equal_3(grades):
    god_grades = []
    for grade in grades:
        if grade >= 3:
            god_grades.append(grade)
    return god_grades

if __name__ == '__main__':
    # print_ten_numbers()
    # print_even_numbers_in_range()
    #
    # print_numbers_divisible_by_number_in_range(0, 31, 7)
    # print_random_numbers(5)
    # print(generate_list_of_10_random_numbers())
    # print_each_number_in_list_squared([4, 6, 9, 13])
    print(map_temperature_from_celsius_to_fahrenheit([10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]))
    print(filter_grades_greater_or_equal_3([1, 2, 5, 3, 6, 2]))