from homework import check_if_animal_needs_vaccine


def test_cat_age_one_needs_vaccine():
    assert check_if_animal_needs_vaccine("cat", 1) == True


def test_cat_age_two_needs_vaccine():
    assert check_if_animal_needs_vaccine("cat", 2) == False
