def tree_biggest_number(lst):
    lst.sort(reverse=True)
    return lst[0:3]


def tree_highest_number(list_of_numbers):
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3: ]


if __name__ == '__main__':

    movies = ["Skazani na Showshank", "Zielona mila", "Fores Gump", "Incepcja", "5 element"]

    print(len(movies))
    print(movies[-2])
    print(movies[0])
    movies.append("Test")
    movies.insert(2, "Oni")
    print(movies)

    print(tree_biggest_number([134, 156, 34, 257]))
    print(tree_highest_number([134, 156, 34, 257]))
