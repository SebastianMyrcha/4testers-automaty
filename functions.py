def print_area_of_circle_with_radius(radius):
    area_of_a_circle = 3.1415 * radius ** 2
    print(area_of_a_circle)


def add_two_numbers(a, b):
    return a + b


def squere_of_number(number):
    return number ** 2


def calculate_volume_quboid(a, b, c):
    return a * b * c


def convert_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


def print_halo_for_name_and_city(name: str, city: str):
    print(f'Witaj {name}! Miło Cię widzieć w naszym mieście {city}!')


if __name__ == '__main__':
    print_area_of_circle_with_radius(5)
    print(add_two_numbers(3, 5))
    print(add_two_numbers(True, True))
    print(squere_of_number(0))
    print(squere_of_number(16))
    print(squere_of_number(2.55))
    print(calculate_volume_quboid(3, 5, 7))
    print(convert_celsius_to_fahrenheit(36))
